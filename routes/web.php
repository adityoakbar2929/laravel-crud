<?php

//use App\Http\Controllers\SampahController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SampahController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get("sampah",[SampahController::class,'index']);
Route::post("sampah/create",[SampahController::class,'create']);
Route::get("sampah/{id}/delete",[SampahController::class,'delete']);