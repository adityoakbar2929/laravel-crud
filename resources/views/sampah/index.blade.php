<!DOCTYPE>
<html>
	<head>
	<title> Data Sampah </title>
	<link href="{{asset('css/uikit.css')}}" rel="stylesheet">
	<link href="{{asset('css/uikit.min.css')}}" rel="stylesheet">

	</head>
	<body>
		<div class="uk-container">
		<div class="uk-alert-danger" uk-alert>
    		<a class="uk-alert-close" uk-close></a>
    		<p>Input Berhasil</p>
		</div>
			<h1> Data Sampah </h1>
		    <br/>
		<button class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-bottom" href="#modal-sections" uk-toggle>Tambah</button>

	<div id="modal-sections" uk-modal>
    	<div class="uk-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
            <h2 class="uk-modal-title">Input Data</h2>
        </div>
        <div class="uk-modal-body">

           <form action="/sampah/create" method="POST" >
           	{{csrf_field()}}
        		<legend class="uk-legend">Kategori</legend>
           	<div class="uk-margin">
            		<select name="kategori_sampah" class="uk-select">
                		<option>Kertas</option>
                		<option>Plastik</option>
            </select>
            <fieldset class="uk-fieldset">
        		<legend class="uk-legend" > Jenis </legend>
           		<div class="uk-margin">
            		<input name="nama_sampah" class="uk-input" type="text" placeholder="Input">
        		</div>
        	</div>

        	<div class="uk-modal-footer uk-text-right">
            	<button class="uk-button uk-button-default uk-modal-close" type="button">Batal</button>
            	<button type="submit" class="uk-button uk-button-primary" type="button" >Simpan</button>
        	</div>
    			
    		</div>
           </form>

        </div>
        
        
	</div>    
		    
		 <div class="uk-card uk-card-default uk-width-5-1@m  uk-margin-medium-top">
		  <div class="uk-card-body">
		    @foreach($data_sampah as $sampah)
				<td><h2>{{$sampah->kategori_sampah}}</h2></td> 
				<td><p>{{$sampah->nama_sampah}}</p></td>
			<a href="/sampah/{{$sampah->id}}/delete" class="uk-button uk-button-text uk-position-small uk-position-bottom-right uk-overlay uk-overlay-default">Hapus</a>
			@endforeach
			</div>
		  </div>
		<script src="{{asset('js/uikit.min.js')}}"></script>	
		
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>


	