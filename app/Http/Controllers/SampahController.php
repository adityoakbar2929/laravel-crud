<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SampahController extends Controller
{
    //
	function index()
	{
		$data_sampah = \App\Models\Sampah::all();
		return view('sampah.index',['data_sampah'=> $data_sampah]);
	}

	function create(Request $request)
	{
		\App\Models\Sampah::create($request->all());
		return redirect('/sampah');
	}
	function delete($id)
	{
		$sampah = \App\Models\Sampah::find($id);
		$sampah->delete();
		return redirect('/sampah')->with('sukses','Data berhasil dihapus');
	}
}
